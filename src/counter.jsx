export default function Counter({ id, inc, dec, updateState, value}) {

    return (
        <div className="counter-container">
            <button className="zero">{value===0 ? "zero":value}</button>
            <button onClick={()=>inc(id)} >➕</button>
            <button onClick={()=>dec(id)}>➖</button>
            <button onClick={()=> updateState(id)}>🗑️</button>
        </div>

    );
}